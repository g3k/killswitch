#include "appwindow.h"
#include "killswitchcontroller.h"

#include <QtGui/QKeyEvent>
#include <QtWebChannel/QWebChannel>

AppWindow::AppWindow(QWidget *parent)
    : QWebEngineView(parent)
{
    setContextMenuPolicy(Qt::NoContextMenu);
}

AppWindow::~AppWindow()
{}

void AppWindow::init()
{
    qputenv("QTWEBENGINE_REMOTE_DEBUGGING", "1234");

    QWebChannel *webChannel = new QWebChannel();
    webChannel->registerObject("appWindow", this);
    page()->setWebChannel(webChannel);

    setUrl(QUrl("qrc:///html/ui.html"));

    setWindowTitle("KillSwitch App");
    resize(400, 400);
}

void AppWindow::connectController(core::KillSwitchController *controller)
{
    connect(this, &AppWindow::enableKillSwitchRequested, controller, &core::KillSwitchController::enable);
    connect(this, &AppWindow::disableKillSwitchRequested, controller, &core::KillSwitchController::disable);
    connect(this, &AppWindow::toggleActivateRequested, controller, [controller] () {
        if (controller->isActive()) controller->deactivate();
        else controller->activate();
    });
    connect(this, &AppWindow::loadFinished, controller, [this, controller] () {
        updateWevPage(controller->isEnabled(), controller->isActive());
    });

    connect(controller, &core::KillSwitchController::changed, this, &AppWindow::updateWevPage);
}

void AppWindow::requestEnableKillSwitch()
{
    emit enableKillSwitchRequested();
}

void AppWindow::requestDisableKillSwitch()
{
    emit disableKillSwitchRequested();
}

void AppWindow::requestToggleActivate()
{
    emit toggleActivateRequested();
}

QString AppWindow::infoStringForState(bool enabled, bool active)
{
    const QString pattern ("KillSwitch is <strong>%1</strong>. Status: <strong>%2</strong>.");
    const QString state_text ((enabled) ? "Enabled" : "Disabled");
    const QString status_text ((active) ? "Active" : "Inactive");
    return pattern.arg(state_text, status_text);
}

void AppWindow::updateWevPage(bool enabled, bool active)
{
    const auto info_text (infoStringForState(enabled, active));
    page()->runJavaScript(QString("$(\"#infotext\").html('%1');").arg(info_text));
}
