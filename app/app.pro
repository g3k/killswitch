QT += core gui webengine webenginewidgets

TARGET = killswitch
TEMPLATE = app
CONFIG += c++14

COREPATH = core

INCLUDEPATH += $$quote(../$$COREPATH/)

SOURCES += \
        main.cpp \
        appwindow.cpp

HEADERS += \
        appwindow.h

DESTDIR =           $$OUT_PWD/../bin
LIB_DIR =           $$OUT_PWD/../lib
OBJECTS_DIR =       $$OUT_PWD/../.obj
MOC_DIR =           $$OUT_PWD/../.moc
PRECOMPILED_DIR =   $$OUT_PWD/../.pch

LIBS += -L$$LIB_DIR

CONFIG (debug, debug|release) {
    PRE_TARGETDEPS += $$LIB_DIR/cored.lib
    LIBS += -lcored
} else {
    PRE_TARGETDEPS += $$LIB_DIR/core.lib
    LIBS += -lcore
}

LIBS += -lwbemuuid

RESOURCES += app.qrc
