#include "appwindow.h"

#include <QtWidgets/QApplication>

#include "killswitchcontroller.h"

int main(int argc, char *argv[])
{
    // init controller
    core::KillSwitchController controller;
    controller.init();

    // init app
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QApplication a(argc, argv);

    // init UI
    AppWindow w;
    w.connectController(&controller);
    w.init();
    w.show();

    return a.exec();
}
