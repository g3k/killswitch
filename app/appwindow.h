#ifndef APPWINDOW_H
#define APPWINDOW_H

#include <QtWebEngineWidgets/QWebEngineView>

namespace core {
class KillSwitchController;
}

/**
 * Main UI widget class
 */
class AppWindow : public QWebEngineView
{
    Q_OBJECT

public:
    AppWindow(QWidget *parent = nullptr);
    ~AppWindow();

    void init();
    void connectController(core::KillSwitchController *controller);

    /*
     * Invokable interface for internal js-calls
     */
    Q_INVOKABLE void requestEnableKillSwitch();
    Q_INVOKABLE void requestDisableKillSwitch();
    Q_INVOKABLE void requestToggleActivate();

signals:
    void enableKillSwitchRequested();
    void disableKillSwitchRequested();
    void toggleActivateRequested();
    void activateKillSwitchRequested();
    void deactivateKillSwitchRequested();

private:
    static QString infoStringForState(bool enabled, bool active);
    void updateWevPage(bool enabled, bool active);
};

#endif // APPWINDOW_H
