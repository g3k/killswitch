#ifndef KILLSWITCHCONTROLLER_H
#define KILLSWITCHCONTROLLER_H

#include <QtCore/QObject>
#include <QtCore/QString>
#include <memory>

namespace core {

class WmiWrapper;


/**
 * Core controller class. Implements kill-switch feature.
 */
class KillSwitchController : public QObject
{
    Q_OBJECT

public:
    explicit KillSwitchController(QObject *parent = nullptr);
    ~KillSwitchController();

    void init();

    void setEnabled(bool value);
    void enable();
    void disable();
    bool isEnabled() const;

    void activate();
    void deactivate();
    bool isActive() const;

signals:
    void enabled();
    void disabled();
    void activated();
    void deactivated();
    void changed(bool, bool);

private:
    void activateKillSwitch();
    void deactivateKillSwitch();

    bool is_enabled_ {true};
    bool is_active_ {false};
    std::unique_ptr<WmiWrapper> wmi_;
    QString interface_id_;
};

} // core

#endif // KILLSWITCHCONTROLLER_H
