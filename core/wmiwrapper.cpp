#include "wmiwrapper.h"

#include <QtCore/QString>
#include <QtCore/QDebug>

#define _WIN32_DCOM
#include <comdef.h>
#include <Wbemidl.h>

namespace core {

// pair of <metric, interface_index>
using IpRouteInfo = std::pair<uint, uint>;

WmiWrapper::WmiWrapper(QObject *parent)
    : QObject(parent)
{}

WmiWrapper::~WmiWrapper()
{
    uninitializeWmi();
}

bool WmiWrapper::initializeWmi()
{
    if (initialized_) uninitializeWmi();

    // initialize COM
    bool res = initCom();
    if (!res) return false;

    // set general COM security levels
    res = initComSecurity();
    if (!res) {
        uninitializeWmi();
        return false;
    }

    // obtain the initial locator to WMI
    res = initLocator();
    if (!res) {
        uninitializeWmi();
        return false;
    }

    // connect to WMI
    res = connectToWmi();
    if (!res) {
        uninitializeWmi();
        return false;
    }

    // set security levels on the proxy
    res = initProxySecurity();
    if (!res) {
        uninitializeWmi();
        return false;
    }

    return true;
}

void WmiWrapper::uninitializeWmi()
{
    if (services_cimv2_)
        services_cimv2_.Release();

    if (services_standardcimv2_)
        services_standardcimv2_.Release();

    if (locator_)
        locator_.Release();

    CoUninitialize();
}

QString WmiWrapper::getGatewayInterfaceId()
{
    // get interface index from the route with the least metric
    const auto index = getInterfaceIndexByMetric();

    // get net adapter id by interface index
    const auto id = getInterfaceId(index);

    return id;
}

bool WmiWrapper::enableInterface(const QString &id)
{
    return setInterfaceEnabled(id, true);
}

bool WmiWrapper::disableInterface(const QString &id)
{
    return setInterfaceEnabled(id, false);
}

bool WmiWrapper::initCom()
{
    // initialize COM single-threaded
    auto hres =  CoInitializeEx(0, COINIT_APARTMENTTHREADED);
    if (FAILED(hres)) {
        qWarning() << "Failed to initialize COM library. Error code:" << QString::number(hres, 16);
        return false;
    }

    return true;
}

bool WmiWrapper::initComSecurity()
{
    auto hres = CoInitializeSecurity(
                NULL,
                -1,                          // COM negotiates service
                NULL,                        // Authentication services
                NULL,                        // Reserved
                RPC_C_AUTHN_LEVEL_DEFAULT,   // Default authentication
                RPC_C_IMP_LEVEL_IMPERSONATE, // Default Impersonation
                NULL,                        // Authentication info
                EOAC_NONE,                   // Additional capabilities
                NULL                         // Reserved
                );

    if (FAILED(hres)) {
        qWarning() << "Failed to initialize security. Error code:" << QString::number(hres, 16);
        return false;
    }
    return true;
}

bool WmiWrapper::initLocator()
{
    IWbemLocator *locator = nullptr;
    auto hres = CoCreateInstance(
                CLSID_WbemAdministrativeLocator,
                0,
                CLSCTX_INPROC_SERVER,
                IID_IWbemLocator, (LPVOID *) &locator);
    locator_ = locator;

    if (FAILED(hres)) {
        qWarning() << "Failed to create IWbemLocator object. Error code:" << QString::number(hres, 16);
        return false;
    }
    return true;
}

bool WmiWrapper::connectToWmi()
{
    // connect to WMI through the IWbemLocator::ConnectServer method
    //  - connect to the local root\cimv2 namespace and obtain services pointer to make IWbemServices calls
    IWbemServices *services = nullptr;
    auto hres = locator_->ConnectServer(_bstr_t(L"root\\cimv2"), NULL, NULL, 0, NULL, 0, 0, &services);
    services_cimv2_ = services;

    if (FAILED(hres)) {
        qWarning() << "Could not connect. Error code:" << QString::number(hres, 16);
        return false;
    }

    //  - connect to the local root\standardcimv2 namespace and obtain services pointer to make IWbemServices calls
    hres = locator_->ConnectServer(_bstr_t(L"root\\standardcimv2"), NULL, NULL, 0, NULL, 0, 0, &services);
    services_standardcimv2_ = services;

    if (FAILED(hres)) {
        qWarning() << "Could not connect. Error code:" << QString::number(hres, 16);
        return false;
    }
    return true;
}

bool WmiWrapper::initProxySecurity()
{
    auto setProxySecurity = [] (IWbemServices *services) {
        return CoSetProxyBlanket(services,                     // Indicates the proxy to set
                                 RPC_C_AUTHN_WINNT,            // RPC_C_AUTHN_xxx
                                 RPC_C_AUTHZ_NONE,             // RPC_C_AUTHZ_xxx
                                 NULL,                         // Server principal name
                                 RPC_C_AUTHN_LEVEL_CALL,       // RPC_C_AUTHN_LEVEL_xxx
                                 RPC_C_IMP_LEVEL_IMPERSONATE,  // RPC_C_IMP_LEVEL_xxx
                                 NULL,                         // client identity
                                 EOAC_NONE);                   // proxy capabilities
    };

    auto hres = setProxySecurity(services_cimv2_);

    if (FAILED(hres)) {
        qWarning() << "Could not set proxy blanket. Error code:" << QString::number(hres, 16);
        return false;
    }

    hres = setProxySecurity(services_standardcimv2_);

    if (FAILED(hres)) {
        qWarning() << "Could not set proxy blanket. Error code:" << QString::number(hres, 16);
        return false;
    }

    return true;
}

uint WmiWrapper::getInterfaceIndexByMetric()
{
    // query routes
    CComPtr<IEnumWbemClassObject> enumerator;
    auto hr = services_cimv2_->ExecQuery(L"WQL", L"SELECT InterfaceIndex, Metric1 FROM Win32_IP4RouteTable",
                                         WBEM_FLAG_FORWARD_ONLY, NULL, &enumerator);

    if (FAILED(hr)) {
        qWarning() << "Exec query failed. Error code:" << QString::number(hr, 16);
        return 0;
    }

    std::vector<IpRouteInfo> infos;
    auto processRoute = [&infos] (CComPtr<IWbemClassObject> &route) {
        // retrieve info pairs from routes query and push'em to container
        _variant_t var_metric;
        auto hr = route->Get(L"Metric1", 0, &var_metric, NULL, NULL);
        if (!SUCCEEDED(hr) || var_metric.vt == VT_NULL) return;

        _variant_t var_index;
        hr = route->Get(L"InterfaceIndex", 0, &var_index, NULL, NULL);
        if (!SUCCEEDED(hr) || var_index.vt == VT_NULL) return;

        infos.emplace_back(uint(var_metric.intVal), uint(var_index.intVal));
    };

    // process routs query
    hr = WBEM_S_NO_ERROR;
    while (WBEM_S_NO_ERROR == hr) {
        ULONG ret_count;
        CComPtr<IWbemClassObject> route;
        hr = enumerator->Next(WBEM_INFINITE, 1L, &route, &ret_count);

        if (!SUCCEEDED(hr) || !ret_count) continue;

        processRoute(route);
    }

    if (infos.empty()) return 0;

    // sort infos by metric
    std::sort(infos.begin(), infos.end(), [] (const IpRouteInfo &lhs, const IpRouteInfo &rhs) {
        return lhs.first < rhs.first;
    });

    // return interface index
    return infos.front().second;
}

QString WmiWrapper::getInterfaceId(uint interface_index)
{
    // TODO: duplicate code refactoring

    // get network adapter DeviceID by interface index
    const auto query_wstr = QString("SELECT DeviceID FROM MSFT_NetAdapter WHERE InterfaceIndex=\"%1\"").arg(interface_index);

    CComPtr<IEnumWbemClassObject> enumerator;
    auto hres = services_standardcimv2_->ExecQuery(L"WQL", _bstr_t(query_wstr.toStdWString().data()), WBEM_FLAG_FORWARD_ONLY, NULL, &enumerator);

    if (FAILED(hres)) {
        qWarning() << "Exec query failed. Error code:" << QString::number(hres, 16);
        return QString();
    }

    // get adapter object
    ULONG ret_count;
    CComPtr<IWbemClassObject> adapter;
    hres = enumerator->Next(WBEM_INFINITE, 1L, &adapter, &ret_count);

    if (FAILED(hres) || !ret_count) {
        qWarning() << "Retrieving adapter failed. Error code:" << QString::number(hres, 16);
        return QString();
    }

    // get adapter object path
    _variant_t var_device_id;
    hres = adapter->Get(_bstr_t(L"DeviceID"), 0, &var_device_id, NULL, NULL);

    if (FAILED(hres)) {
        qWarning() << "DeviceID query failed. Error code:" << QString::number(hres, 16);
        return QString();
    }

    return QString::fromStdWString(var_device_id.bstrVal);
}

bool WmiWrapper::setInterfaceEnabled(const QString &id, bool enabled)
{
    if (id.isEmpty()) return false;

    // get network adapter object by interface index
    const auto query_wstr = QString("SELECT * FROM MSFT_NetAdapter WHERE DeviceId=\"%1\"").arg(id);

    CComPtr<IEnumWbemClassObject> enumerator;
    auto hr = services_standardcimv2_->ExecQuery(L"WQL", _bstr_t(query_wstr.toStdWString().data()), WBEM_FLAG_FORWARD_ONLY, NULL, &enumerator);

    if (FAILED(hr)) {
        qWarning() << "Exec query failed. Error code:" << QString::number(hr, 16);
        return false;
    }

    // get adapter object
    ULONG ret_count;
    CComPtr<IWbemClassObject> adapter;
    hr = enumerator->Next(WBEM_INFINITE, 1L, &adapter, &ret_count);

    if (FAILED(hr) || !ret_count) {
        qWarning() << "Retrieving adapter failed. Error code:" << QString::number(hr, 16);
        return false;
    }

    // get adapter object path
    _variant_t var_object_path;
    hr = adapter->Get(_bstr_t(L"__PATH"), 0, &var_object_path, NULL, NULL);

    if (FAILED(hr)) {
        qWarning() << "PATH query failed. Error code:" << QString::number(hr, 16);
        return false;
    }

    // exec Enable/Disable method
    const _bstr_t method_name((enabled) ? L"Enable" : L"Disable");
    hr = services_standardcimv2_->ExecMethod(var_object_path.bstrVal, method_name, 0, NULL, NULL, NULL, NULL);

    if (FAILED(hr)) {
        qWarning() << "Exec method failed. Error code:" << QString::number(hr, 16);
        return false;
    }

    return true;
}

} // core
