#include "killswitchcontroller.h"
#include "wmiwrapper.h"

namespace core {

KillSwitchController::KillSwitchController(QObject *parent)
    : QObject(parent)
    , wmi_(new WmiWrapper)
{}

KillSwitchController::~KillSwitchController()
{}

void KillSwitchController::init()
{
    wmi_->initializeWmi();

    auto changedNotification = [this] () {
        emit changed(isEnabled(), isActive());
    };

    connect(this, &KillSwitchController::enabled, this, changedNotification);
    connect(this, &KillSwitchController::disabled, this, changedNotification);
    connect(this, &KillSwitchController::activated, this, changedNotification);
    connect(this, &KillSwitchController::deactivated, this, changedNotification);
}

bool KillSwitchController::isEnabled() const
{
    return is_enabled_;
}

void KillSwitchController::activate()
{
    activateKillSwitch();
}

void KillSwitchController::deactivate()
{
    deactivateKillSwitch();
}

void KillSwitchController::setEnabled(bool value)
{
    if (is_enabled_ == value) return;

    is_enabled_ = value;
    if (is_enabled_) {
        emit enabled();
    } else {
        // deactivate on set enabled false
        deactivateKillSwitch();
        emit disabled();
    }
}

void KillSwitchController::enable()
{
    setEnabled(true);
}

void KillSwitchController::disable()
{
    setEnabled(false);
}

bool KillSwitchController::isActive() const
{
    return is_active_;
}

void KillSwitchController::activateKillSwitch()
{
    if (is_active_ || !is_enabled_) return;

    // store target interface id
    interface_id_ = wmi_->getGatewayInterfaceId();

    // try to disable interface
    is_active_ = wmi_->disableInterface(interface_id_);
    if (is_active_) emit activated();
}

void KillSwitchController::deactivateKillSwitch()
{
    if (!is_active_) return;

    // try to enable interface
    is_active_ = !wmi_->enableInterface(interface_id_);
    if (!is_active_) emit deactivated();
}

} // core
