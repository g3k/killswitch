QT += core

TEMPLATE = lib
CONFIG += staticlib c++14

TARGET = core
CONFIG(debug, debug|release) {
    TARGET = $$join(TARGET,,,d)
}

QMAKE_CXXFLAGS -= -Zc:strictStrings

SOURCES += \
    killswitchcontroller.cpp \
    wmiwrapper.cpp

HEADERS += \
    killswitchcontroller.h \
    wmiwrapper.h

DESTDIR =           $$OUT_PWD/../lib
OBJECTS_DIR =       $$OUT_PWD/../.obj
MOC_DIR =           $$OUT_PWD/../.moc
PRECOMPILED_DIR =   $$OUT_PWD/../.pch
