#ifndef WMIWRAPPER_H
#define WMIWRAPPER_H

#include <QtCore/QObject>
#include <QtCore/QString>
#include <atlbase.h>

struct IWbemLocator;
struct IWbemServices;

namespace core {

/**
 * Wrapper class, provides access to Wmi services.
 * Minimum supported client: Windows 8, since MSFT_NetAdapter class is used.
 * To be able to use interface modification app should be Run as Administrator.
 */
class WmiWrapper : public QObject
{
    Q_OBJECT

public:
    explicit WmiWrapper(QObject *parent = nullptr);
    ~WmiWrapper();

    bool initializeWmi();
    void uninitializeWmi();

    QString getGatewayInterfaceId();
    bool enableInterface(const QString &id);
    bool disableInterface(const QString &id);

private:
    bool initCom();
    bool initComSecurity();
    bool initLocator();
    bool connectToWmi();
    bool initProxySecurity();
    uint getInterfaceIndexByMetric();
    QString getInterfaceId(uint interface_index);
    bool setInterfaceEnabled(const QString &id, bool enabled);

    bool initialized_ {false};
    CComPtr<IWbemLocator> locator_ {nullptr};
    CComPtr<IWbemServices> services_cimv2_ {nullptr};
    CComPtr<IWbemServices> services_standardcimv2_ {nullptr};
};

} // core

#endif // WMIWRAPPER_H
